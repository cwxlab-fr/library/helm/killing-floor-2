{{- define "default.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{- define "default.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{- define "default.selectorLabels" -}}
app.kubernetes.io/name: {{ include "default.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{- define "default.labels" -}}
helm.sh: {{ include "default.chart" . }}
{{ include "default.selectorLabels" .}}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{- define "default.steamcmd.image.repository" -}}
steamcmd/steamcmd
{{- end }}

{{- define "default.steamcmd.image.tag" -}}
ubuntu-22
{{- end }}

{{- define "default.services.webadmin.name" -}}
{{ include "default.name" . }}-webadmin
{{- end }}

{{- define "default.services.webadmin.ports.webadminPort.name" -}}
{{ include "default.name" . }}-webadminport
{{- end }}

{{- define "default.services.game.name" -}}
{{ include "default.name" . }}-game
{{- end }}

{{- define "default.services.game.ports.gamePort.name" -}}
{{ include "default.name" . }}-gameport
{{- end }}

{{- define "default.services.game.ports.queryPort.name" -}}
{{ include "default.name" . }}-queryport
{{- end }}

{{- define "default.services.game.ports.steamPort.name" -}}
{{ include "default.name" . }}-steamport
{{- end }}

{{- define "default.DataDir" -}}
/opt/{{ include "default.name" . }}
{{- end }}

{{- define "default.ConfigSubDirName" -}}
{{ .Release.Name }}
{{- end }}

{{- define "default.ConfigSubDir" -}}
{{ include "default.DataDir" . }}/KFGame/Config/{{ include "default.ConfigSubDirName" . }}
{{- end }}
